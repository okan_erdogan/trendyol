package com.trendyol.shoppingcart;

import com.trendyol.shoppingcart.entities.Category;
import com.trendyol.shoppingcart.entities.Product;
import com.trendyol.shoppingcart.entities.ShoppingCart;
import com.trendyol.shoppingcart.entities.discounts.DiscountMethod;
import com.trendyol.shoppingcart.presenter.ShoppingCartPresenter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Okan ERDOGAN on 2019-10-01.
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class ShoppingCartPresenterTest {


    ShoppingCartPresenter shoppingCartPresenter;
    ShoppingCart shoppingCart;
    Product product;

    @Mock
    Category category;

    @Before
    public void init() {
        shoppingCartPresenter = new ShoppingCartPresenter();
        shoppingCartPresenter.createProducts();

        shoppingCart = new ShoppingCart();

        product = new Product("Pierre Cardin (Erkek Gömlek)", 89.95, category);
    }

    @Test
    public void createProductsTest() {
        shoppingCartPresenter.createProducts();

        Assert.assertEquals(11, shoppingCartPresenter.getProducts().size());
        for (Product product : shoppingCartPresenter.getProducts()) {
            Assert.assertEquals(0, product.getCount());
            Assert.assertFalse(product.isSelected());
        }

    }

    @Test
    public void createDiscountsTest() {
        shoppingCartPresenter.createDiscounts(shoppingCart);

        Assert.assertEquals(4, shoppingCart.getDiscountList().size());
        Assert.assertNotNull(shoppingCart.getDiscountList().getFirst());
        Assert.assertNotNull(shoppingCart.getDiscountList().getLast());
        Assert.assertEquals(DiscountMethod.Campaign, shoppingCart.getDiscountList().getFirst().discountMethod);
        Assert.assertEquals(DiscountMethod.Coupon, shoppingCart.getDiscountList().getLast().discountMethod);

    }

    @Test
    public void updateProductListTest() {
        product.setCount(10);
        product.setSelected(true);
        shoppingCart.addItem(product);
        shoppingCartPresenter.updateProductList(shoppingCart);


        for (Product product : shoppingCartPresenter.getProducts()) {
            if (shoppingCart.getProductsMap().get(product.getTitle()) != null) {
                Assert.assertEquals(10, product.getCount());
                Assert.assertEquals(true, product.isSelected());
                Assert.assertEquals(89.95, product.getPrice(), 0);
                Assert.assertEquals("Pierre Cardin (Erkek Gömlek)", product.getTitle());
            } else {
                Assert.assertEquals(0, product.getCount());
                Assert.assertEquals(false, product.isSelected());
            }
        }
    }


}
