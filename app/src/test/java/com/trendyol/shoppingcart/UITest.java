package com.trendyol.shoppingcart;

import com.trendyol.shoppingcart.entities.Product;
import com.trendyol.shoppingcart.ui.BaseActivity;
import com.trendyol.shoppingcart.ui.ProductListFragment;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

/**
 * Created by Okan ERDOGAN on 2019-10-01.
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class UITest {


    @Mock
    BaseActivity baseActivity;

    @Mock
    ProductListFragment productListFragment;

    @Mock
    Product product;

    @Before
    public void init() {
    }

    @Test
    public void setStatusBarColorTest() {

        baseActivity.setStatusBarColor();
        verify(baseActivity, atLeastOnce()).setStatusBarColor();
    }

    @Test
    public void productClickedTest() {
        productListFragment.productClicked(product, false);
        verify(productListFragment, atLeastOnce()).productClicked(product, false);
    }

    @Test
    public void showToastWarnTest() {
        productListFragment.showToastWarn(false);
        verify(productListFragment, atLeastOnce()).showToastWarn(false);
    }

    @Test
    public void productLonClickedTest() {
        productListFragment.productLongClicked(product);
        verify(productListFragment, atLeastOnce()).productLongClicked(product);
    }


}
