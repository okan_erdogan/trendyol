package com.trendyol.shoppingcart;

import com.trendyol.shoppingcart.entities.Category;
import com.trendyol.shoppingcart.entities.Product;
import com.trendyol.shoppingcart.entities.ShoppingCart;
import com.trendyol.shoppingcart.entities.discounts.Campaign;
import com.trendyol.shoppingcart.entities.discounts.Coupon;
import com.trendyol.shoppingcart.entities.discounts.DiscountType;
import com.trendyol.shoppingcart.presenter.ShoppingCartPresenter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.LinkedList;
import java.util.TreeMap;

/**
 * Created by Okan ERDOGAN on 2019-10-01.
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class ShoppingCartTest {

    ShoppingCartPresenter shoppingCartPresenter;
    ShoppingCart shoppingCart;
    Product product, product2, product3, product4;
    Category category, category2, category3;

    @Before
    public void init() {
        shoppingCart = new ShoppingCart();
        this.shoppingCartPresenter = new ShoppingCartPresenter();
        category = new Category("Ayakkabı", R.drawable.shoes_icon);
        category2 = new Category("Elektronik", R.drawable.electronic_icon);
        category3 = new Category("Giyim", R.drawable.clothes_icon);

        product = new Product("Reebok (Erkek Günlük Ayakkabı)", 392.99, category);
        product3 = new Product("Du Jour Paris (Siyah Kadın Topuklu Ayakkabı)", 59.99, category);
        product2 = new Product("Xiaomi (Redmi Note7 3GB Ram)", 1442, category2);
        product4 = new Product("Pierre Cardin (Erkek Gömlek)", 89.95, category3);

    }

    @Test
    public void getDeliveryCostTest() {
        shoppingCart.addItem(product);
        Assert.assertEquals(10.99, shoppingCart.getDeliveryCost(), 0);
        Assert.assertNotEquals(15.99, shoppingCart.getDeliveryCost(), 0);

        shoppingCart.addItem(product2);
        shoppingCart.addItem(product2);
        shoppingCart.addItem(product2);
        Assert.assertEquals(18.99, shoppingCart.getDeliveryCost(), 0.1);

        shoppingCart.addItem(product3);
        Assert.assertEquals(21.99, shoppingCart.getDeliveryCost(), 0.1);
        shoppingCart.addItem(product2);
        shoppingCart.addItem(product2);
        Assert.assertEquals(21.99, shoppingCart.getDeliveryCost(), 0.1);
    }

    @Test
    public void attachCampaignDiscountTest() {
        shoppingCart.addItem(product);
        shoppingCart.addItem(product2);
        shoppingCart.addItem(product3);
        product4.setCount(15);
        shoppingCart.addItem(product4);

        shoppingCartPresenter.createProducts();
        shoppingCartPresenter.createDiscounts(shoppingCart);

        TreeMap<Category, LinkedList<Product>> productsAccordingToCategoryMap = shoppingCart.formatShoppingCartProductsAccordingToCategory();
        shoppingCart.attachCampaignDiscount(productsAccordingToCategoryMap);

        Assert.assertEquals(0, category.getAddedDiscountMap().size());
        Assert.assertEquals(0, category2.getAddedDiscountMap().size());
        Assert.assertEquals(2, category3.getAddedDiscountMap().size());
    }

    @Test
    public void applyDiscountTest() {
        Campaign campaign1 = new Campaign(category2, 20.0, 3, DiscountType.Rate);
        shoppingCart.applyDiscounts(campaign1);

        Assert.assertNotNull(shoppingCart.getDiscountList());
        Assert.assertEquals(campaign1, shoppingCart.getDiscountList().getFirst());
    }

    @Test
    public void calculateItemCount() {
        product4.setCount(15);
        shoppingCart.addItem(product4);
        Assert.assertEquals(15, shoppingCart.calculateItemCount());
    }


    @Test
    public void getCouponDiscountsTest() {
        Assert.assertEquals(0, shoppingCart.getCouponDiscounts(100), 0);

        Coupon coupon = new Coupon(100, 10, DiscountType.Rate);
        shoppingCart.applyDiscounts(coupon);

        Assert.assertEquals(0, shoppingCart.getCouponDiscounts(100), 0);
        Assert.assertEquals(15, shoppingCart.getCouponDiscounts(150), 0);

    }

    @Test
    public void getTotalAmountAfterDiscountsTest() {
        Assert.assertEquals(130, shoppingCart.getTotalAmountAfterDiscounts(150, 20), 0);
    }
}
