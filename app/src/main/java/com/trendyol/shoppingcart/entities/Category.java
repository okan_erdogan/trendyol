package com.trendyol.shoppingcart.entities;

import com.trendyol.shoppingcart.entities.discounts.DiscountType;

import java.io.Serializable;
import java.util.TreeMap;

/**
 * Created by Okan ERDOGAN on 2019-09-29.
 */
public class Category implements Serializable, Comparable<Category> {
    private String title;
    private int icon;
    private Category topCategory;
    private TreeMap<DiscountType, Double> addedDiscountMap;

    public Category(String title, int icon) {
        this.icon = icon;
        this.title = title;
        this.addedDiscountMap = new TreeMap<>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getTopCategory() {
        return topCategory;
    }

    public void setTopCategory(Category topCategory) {
        this.topCategory = topCategory;
    }

    public int getIcon() {
        return icon;
    }

    public TreeMap<DiscountType, Double> getAddedDiscountMap() {
        return addedDiscountMap;
    }

    @Override
    public int compareTo(Category category) {
        if (category.getTitle().equals(title)) {
            return 0;
        } else {
            return 1;
        }
    }
}
