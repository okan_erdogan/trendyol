package com.trendyol.shoppingcart.entities.discounts;

/**
 * Created by Okan ERDOGAN on 2019-09-29.
 */
public class Coupon extends Discount {
    private double shoppingAmount;
    private double discountRate;

    public Coupon(double shoppingAmount, double discountRate, DiscountType discountType) {
        this.shoppingAmount = shoppingAmount;
        this.discountRate = discountRate;
        this.discountType = discountType;
        this.discountMethod = DiscountMethod.Coupon;
    }

    public double getShoppingAmount() {
        return shoppingAmount;
    }

    public double getDiscountRate() {
        return discountRate;
    }
}
