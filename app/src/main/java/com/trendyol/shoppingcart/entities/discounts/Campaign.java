package com.trendyol.shoppingcart.entities.discounts;

import com.trendyol.shoppingcart.entities.Category;

/**
 * Created by Okan ERDOGAN on 2019-09-29.
 */
public class Campaign extends Discount {

    private Category category;
    private double discountRate;
    private int productCount;

    public Campaign(Category category, double discountRate, int productCount, DiscountType discountType) {
        this.category = category;
        this.discountRate = discountRate;
        this.productCount = productCount;
        this.discountType = discountType;
        this.discountMethod = DiscountMethod.Campaign;
    }

    public Category getCategory() {
        return category;
    }

    public double getDiscountRate() {
        return discountRate;
    }

    public int getProductCount() {
        return productCount;
    }
}
