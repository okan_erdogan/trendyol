package com.trendyol.shoppingcart.entities.discounts;

/**
 * Created by Okan ERDOGAN on 2019-10-01.
 */
public enum DiscountMethod {
    Campaign, Coupon;
}
