package com.trendyol.shoppingcart.entities.discounts;

import java.io.Serializable;

/**
 * Created by Okan ERDOGAN on 2019-09-29.
 */
public class Discount implements Serializable {
    public DiscountType discountType;
    public DiscountMethod discountMethod;
}
