package com.trendyol.shoppingcart.entities.discounts;

/**
 * Created by Okan ERDOGAN on 2019-09-29.
 */
public enum DiscountType {
    Rate, Amount
}
