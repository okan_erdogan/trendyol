package com.trendyol.shoppingcart.entities;

import com.trendyol.shoppingcart.constants.DeliveryConstants;
import com.trendyol.shoppingcart.entities.discounts.Campaign;
import com.trendyol.shoppingcart.entities.discounts.Coupon;
import com.trendyol.shoppingcart.entities.discounts.Discount;
import com.trendyol.shoppingcart.entities.discounts.DiscountMethod;
import com.trendyol.shoppingcart.entities.discounts.DiscountType;
import com.trendyol.shoppingcart.util.DeliveryCostCalculator;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.TreeMap;

/**
 * Created by Okan ERDOGAN on 2019-09-29.
 */
public class ShoppingCart implements Serializable {
    private HashMap<String, Product> productTreeMap;
    private LinkedList<Discount> discountList;
    private double deliveryCost;

    public ShoppingCart() {
        this.discountList = new LinkedList<>();
        this.productTreeMap = new HashMap<>();
    }

    public HashMap<String, Product> getProductsMap() {
        return productTreeMap;
    }

    public LinkedList<Discount> getDiscountList() {
        return discountList;
    }

    public void addItem(Product products) {
        this.productTreeMap.put(products.getTitle(), products);
    }

    public int calculateItemCount() {
        int itemCount = 0;
        for (Product product : getProductsMap().values()) {
            itemCount = itemCount + product.getCount();
        }

        return itemCount;
    }

    public void applyDiscounts(Discount... discounts) {
        for (Discount discount : discounts) {
            this.discountList.add(discount);
        }
    }


    public double getDeliveryCost() {
        this.deliveryCost = DeliveryCostCalculator.calculateDeliveryCost(this, DeliveryConstants.COST_PER_DELIVERY, DeliveryConstants.COST_PER_PRODUCT, DeliveryConstants.COST_FIXED);
        return deliveryCost;
    }

    public void attachCampaignDiscount(TreeMap<Category, LinkedList<Product>> productsAccordingToCategoryMap) {
        for (Discount discount : this.discountList) {
            if (discount.discountMethod == DiscountMethod.Campaign) {
                Campaign campaign = (Campaign) discount;
                for (Category category : productsAccordingToCategoryMap.keySet()) {
                    if (campaign.getCategory().getTitle().equals(category.getTitle())) {
                        int productCountAccordingCategory = 0;

                        for (Product product : productsAccordingToCategoryMap.get(category)) {
                            productCountAccordingCategory = productCountAccordingCategory + product.getCount();
                        }

                        if (campaign.getProductCount() < productCountAccordingCategory) {
                            switch (campaign.discountType) {
                                case Rate:
                                    if (category.getAddedDiscountMap().containsKey(DiscountType.Rate)) {
                                        if (category.getAddedDiscountMap().get(DiscountType.Rate) < campaign.getDiscountRate()) {
                                            category.getAddedDiscountMap().put(DiscountType.Rate, campaign.getDiscountRate());
                                        }
                                    } else {
                                        category.getAddedDiscountMap().put(DiscountType.Rate, campaign.getDiscountRate());
                                    }

                                    break;
                                case Amount:
                                    if (category.getAddedDiscountMap().containsKey(DiscountType.Amount)) {
                                        if (category.getAddedDiscountMap().get(DiscountType.Amount) < campaign.getDiscountRate()) {
                                            category.getAddedDiscountMap().put(DiscountType.Amount, campaign.getDiscountRate());
                                        }
                                    } else {
                                        category.getAddedDiscountMap().put(DiscountType.Amount, campaign.getDiscountRate());
                                    }

                                    break;
                            }
                        }
                    }
                }
            }
        }
    }

    public double getCouponDiscounts(double totalAmount) {
        double couponDiscount = 0;
        for (Discount discount : this.discountList) {
            if (discount.discountMethod == DiscountMethod.Coupon) {
                Coupon coupon = (Coupon) discount;
                if (totalAmount > coupon.getShoppingAmount()) {
                    switch (coupon.discountType) {
                        case Rate:
                            couponDiscount = couponDiscount + totalAmount * coupon.getDiscountRate() / 100;
                            break;
                        case Amount:
                            couponDiscount = couponDiscount + coupon.getDiscountRate();
                            break;

                    }
                }
            }
        }
        return couponDiscount;
    }


    public double getTotalAmountAfterDiscounts(double totalAmount, double totalCouponDiscount) {
        double totalAmountAfterDiscounts = totalAmount - totalCouponDiscount;
        return totalAmountAfterDiscounts;
    }

    public TreeMap<Category, LinkedList<Product>> formatShoppingCartProductsAccordingToCategory() {
        TreeMap<Category, LinkedList<Product>> productsAccordingToCategoryMap = new TreeMap<>();


        for (Product product : this.productTreeMap.values()) {
            if (!productsAccordingToCategoryMap.containsKey(product.getCategory())) {
                productsAccordingToCategoryMap.put(product.getCategory(), new LinkedList<Product>());
            }

            LinkedList linkedList = productsAccordingToCategoryMap.get(product.getCategory());
            linkedList.add(product);

        }

        return productsAccordingToCategoryMap;
    }


    public String print() {
        String summary = "";
        double totalPrice = 0;
        TreeMap<Category, LinkedList<Product>> productsAccordingToCategoryMap = formatShoppingCartProductsAccordingToCategory();
        this.attachCampaignDiscount(productsAccordingToCategoryMap);

        double totalCategoryDiscount = 0;
        for (Category category : productsAccordingToCategoryMap.keySet()) {
            double categoryPrice = 0;
            summary = summary + category.getTitle() + '\n';
            for (Product product : productsAccordingToCategoryMap.get(category)) {
                double productPrice = product.getCount() * product.getPrice();
                categoryPrice = categoryPrice + productPrice;
                totalPrice = totalPrice + productPrice;
                summary = summary + product.getCount() + " x " + product.getTitle() + "(" + String.format("%.2f", product.getPrice()) + " " + product.getCurrency() + ")" + '\n' + String.format("%.2f", productPrice) + " " + product.getCurrency() + '\n' + '\n';
            }

            double categoryDiscount = calculateCategoryDiscount(category, categoryPrice);
            totalCategoryDiscount = totalCategoryDiscount + categoryDiscount;
        }


        double totalPriceAfterCampaginDiscount = totalPrice - totalCategoryDiscount;
        double totalCouponDiscount = getCouponDiscounts(totalPriceAfterCampaginDiscount);
        double totalPriceAfterAllDiscount = getTotalAmountAfterDiscounts(totalPriceAfterCampaginDiscount, totalCouponDiscount);


        summary = summary + "Toplam Fiyat : " + String.format("%.2f", totalPrice) + " TL" + '\n';
        summary = summary + "Toplam Kampanya İndirimi : " + String.format("%.2f", totalCategoryDiscount) + " TL" + '\n';
        summary = summary + "Toplam Kupon İndirimi : " + String.format("%.2f", totalCouponDiscount) + " TL" + '\n';
        summary = summary + "Toplam Fiyat (İndirimlerden Sonra) : " + String.format("%.2f", totalPriceAfterAllDiscount) + " TL" + '\n' + '\n';

        summary = summary + "Kargo Ücreti : " + String.format("%.2f", getDeliveryCost()) + " TL" + '\n';
        summary = summary + "Ödenecek Miktar : " + String.format("%.2f", totalPriceAfterAllDiscount + getDeliveryCost()) + " TL";

        return summary;
    }


    private double calculateCategoryDiscount(Category category, double categoryPrice) {
        double categoryDiscount = 0;
        for (DiscountType discountType : category.getAddedDiscountMap().keySet()) {
            double categoryDiscountRate = category.getAddedDiscountMap().get(discountType);
            switch (discountType) {
                case Rate:
                    categoryDiscount = categoryDiscount + (categoryPrice * categoryDiscountRate / 100);
                    break;
                case Amount:
                    categoryDiscount = categoryDiscount + categoryDiscountRate;
                    break;
            }
        }

        return categoryDiscount;
    }
}
