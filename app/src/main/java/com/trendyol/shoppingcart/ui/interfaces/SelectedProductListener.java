package com.trendyol.shoppingcart.ui.interfaces;

import com.trendyol.shoppingcart.entities.Product;

/**
 * Created by Okan ERDOGAN on 2019-09-30.
 */
public interface SelectedProductListener {
    public void productClicked(Product product,boolean isSelected);
    public void productLongClicked(Product product);

}
