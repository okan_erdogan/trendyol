package com.trendyol.shoppingcart.ui.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trendyol.shoppingcart.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Okan ERDOGAN on 2019-09-30.
 */
public class ProductsViewHolder extends RecyclerView.ViewHolder {

    final ImageView productIcon;
    final TextView productName, categoryName, price, itemCount;
    final LinearLayout productWrapper;

    public ProductsViewHolder(@NonNull View itemView) {
        super(itemView);
        this.productIcon = itemView.findViewById(R.id.productIcon);
        this.productName = itemView.findViewById(R.id.productName);
        this.categoryName = itemView.findViewById(R.id.categoryName);
        this.price = itemView.findViewById(R.id.price);
        this.itemCount = itemView.findViewById(R.id.item_count);
        this.productWrapper = itemView.findViewById(R.id.productWrapper);
    }
}
