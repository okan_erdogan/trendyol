package com.trendyol.shoppingcart.ui;

import android.os.Bundle;

import com.trendyol.shoppingcart.R;

/**
 * Created by Okan ERDOGAN on 2019-09-30.
 */
public class ShoppingCartActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart);
    }
}
