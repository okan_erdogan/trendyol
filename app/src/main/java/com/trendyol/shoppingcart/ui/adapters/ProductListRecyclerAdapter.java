package com.trendyol.shoppingcart.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trendyol.shoppingcart.R;
import com.trendyol.shoppingcart.entities.Product;
import com.trendyol.shoppingcart.ui.interfaces.SelectedProductListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Okan ERDOGAN on 2019-09-30.
 */
public class ProductListRecyclerAdapter extends RecyclerView.Adapter {

    private List<Product> productList;
    private SelectedProductListener selectedProductListener;

    public ProductListRecyclerAdapter(List<Product> productList, SelectedProductListener selectedProductListener) {
        this.productList = productList;
        this.selectedProductListener = selectedProductListener;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemProduct = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        return new ProductsViewHolder(itemProduct);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ProductsViewHolder) {
            final ProductsViewHolder productsViewHolder = (ProductsViewHolder) holder;

            final Product boundProduct = productList.get(position);
            if (boundProduct != null) {

                productsViewHolder.productName.setText(boundProduct.getTitle());
                productsViewHolder.categoryName.setText(boundProduct.getCategory().getTitle());
                productsViewHolder.price.setText(boundProduct.getPrice() + boundProduct.getCurrency());
                productsViewHolder.productIcon.setImageResource(boundProduct.getCategory().getIcon());
                productsViewHolder.productWrapper.setSelected(boundProduct.isSelected());


                if (boundProduct.getCount() > 0) {
                    productsViewHolder.itemCount.setVisibility(View.VISIBLE);
                    productsViewHolder.itemCount.setText(String.valueOf(boundProduct.getCount()));
                } else {
                    productsViewHolder.itemCount.setVisibility(View.INVISIBLE);
                }

                productsViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        selectedProductListener.productClicked(boundProduct, true);
                    }
                });

                productsViewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {

                        selectedProductListener.productLongClicked((boundProduct));
                        return false;
                    }
                });
            }

        } else {
            return;
        }
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

}
