package com.trendyol.shoppingcart.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.trendyol.shoppingcart.R;
import com.trendyol.shoppingcart.entities.ShoppingCart;

import androidx.fragment.app.Fragment;

/**
 * Created by Okan ERDOGAN on 2019-09-30.
 */
public class ShoppingCartFragment extends Fragment {

    private ShoppingCart shoppingCart;
    private EditText summary;
    private Button confirm, decline;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_shopping_cart, container, false);

        v.findViewById(R.id.shoppingCart).setVisibility(View.INVISIBLE);

        this.shoppingCart = (ShoppingCart) getActivity().getIntent().getSerializableExtra(ProductListFragment.SHOPPING_CART_EXTRA);

        this.summary = v.findViewById(R.id.summary);
        this.summary.setText(this.shoppingCart.print());

        this.confirm = v.findViewById(R.id.confirm);
        this.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog("Bilgilendirme", "Satın alma talebiniz alındı.", "Tamam", null);
            }
        });
        this.decline = v.findViewById(R.id.decline);
        this.decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog("Uyarı", "Bu işlem sonunda sepetinizdeki ürünler temizlenecektir. Sepetinizi düzenlemek için geri butonuna basınız.", "Temizle", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }
        });

        return v;
    }

    private void showDialog(String title, String message, String confirmButtonText, DialogInterface.OnClickListener cancelClick) {
        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, confirmButtonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finishActivity();
                    }
                });

        if (cancelClick != null) {
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Kapat",
                    cancelClick);
        }

        alertDialog.show();
    }


    private void finishActivity() {
        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();
    }
}
