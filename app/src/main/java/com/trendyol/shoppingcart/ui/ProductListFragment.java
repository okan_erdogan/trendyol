package com.trendyol.shoppingcart.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.trendyol.shoppingcart.R;
import com.trendyol.shoppingcart.entities.Product;
import com.trendyol.shoppingcart.entities.ShoppingCart;
import com.trendyol.shoppingcart.presenter.ShoppingCartPresenter;
import com.trendyol.shoppingcart.ui.adapters.ProductListRecyclerAdapter;
import com.trendyol.shoppingcart.ui.interfaces.SelectedProductListener;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Okan ERDOGAN on 2019-09-30.
 */
public class ProductListFragment extends Fragment implements SelectedProductListener {
    private RecyclerView productList;
    private ProductListRecyclerAdapter productListRecyclerAdapter;
    private ShoppingCartPresenter shoppingCartPresenter;
    private TextView count;
    private ImageView shoppingCartView;
    public static int REQUEST_CODE = 1;

    private ShoppingCart shoppingCart;
    private boolean isToastWarnShown = false;

    public static final String SHOPPING_CART_EXTRA = "SHOPPING_CART_EXTRA";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_product_list, container, false);
        this.productList = v.findViewById(R.id.productList);
        this.count = v.findViewById(R.id.count);
        this.shoppingCartView = v.findViewById(R.id.shoppingCart);

        this.shoppingCartView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startShoppingCartActivity();
            }
        });
        this.productList.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        this.productList.setLayoutManager(new LinearLayoutManager(getActivity()));

        this.shoppingCartPresenter = new ShoppingCartPresenter();
        this.shoppingCart = new ShoppingCart();
        this.productListRecyclerAdapter = new ProductListRecyclerAdapter(shoppingCartPresenter.createProducts(), this);
        this.productList.setAdapter(this.productListRecyclerAdapter);
        this.shoppingCartPresenter.createDiscounts(shoppingCart);
        return v;
    }

    @Override
    public void productClicked(Product product, boolean isSelected) {

        product.setSelected(isSelected);
        if (isSelected) {
            product.increaseCount();
            this.shoppingCart.addItem(product);
        } else {
            if (this.shoppingCart.getProductsMap().containsKey(product.getTitle())) {
                this.shoppingCart.getProductsMap().get(product.getTitle()).decreaseCount();
            }

            if (product.getCount() == 0) {
                this.shoppingCart.getProductsMap().remove(product.getTitle());
            }
        }

        updateshoppingCartCount(shoppingCart);

        shoppingCartPresenter.updateProductList(shoppingCart);
        this.productListRecyclerAdapter.notifyDataSetChanged();

        showToastWarn(isSelected);
    }


    public void showToastWarn(boolean isSelected) {
        if (shoppingCart.getProductsMap().size() == 1 && isSelected && !isToastWarnShown) {
            isToastWarnShown = true;
            Toast.makeText(getContext(), "Seçtiğiniz ürünü listeden kaldırmak için üzerine basılı tutabilirsiniz.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void productLongClicked(Product product) {
        product.setSelected(false);
        product.setCount(0);
        shoppingCartPresenter.updateProductList(shoppingCart);
        this.productListRecyclerAdapter.notifyDataSetChanged();
        this.shoppingCart.getProductsMap().remove(product.getTitle());
        updateshoppingCartCount(this.shoppingCart);
    }


    private void updateshoppingCartCount(ShoppingCart shoppingCart) {
        if (shoppingCart.getProductsMap().size() == 0) {
            this.count.setVisibility(View.INVISIBLE);
        } else {
            this.count.setVisibility(View.VISIBLE);
            this.count.setText(String.valueOf(shoppingCart.calculateItemCount()));
        }
    }

    private void startShoppingCartActivity() {
        Intent intent = new Intent(getActivity(), ShoppingCartActivity.class);
        intent.putExtra(SHOPPING_CART_EXTRA, shoppingCart);
        startActivityForResult(intent, REQUEST_CODE);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            this.shoppingCartPresenter.getProducts().clear();
            this.shoppingCart.getProductsMap().clear();
            updateshoppingCartCount(this.shoppingCart);
            shoppingCartPresenter.createProducts();
            productListRecyclerAdapter.notifyDataSetChanged();
        }
    }
}
