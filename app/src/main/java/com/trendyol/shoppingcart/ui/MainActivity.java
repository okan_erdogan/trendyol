package com.trendyol.shoppingcart.ui;

import android.content.Intent;
import android.os.Bundle;

import com.trendyol.shoppingcart.R;

import androidx.fragment.app.Fragment;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ProductListFragment.REQUEST_CODE) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.productListFragment);
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
