package com.trendyol.shoppingcart.presenter;

import com.trendyol.shoppingcart.R;
import com.trendyol.shoppingcart.entities.Category;
import com.trendyol.shoppingcart.entities.Product;
import com.trendyol.shoppingcart.entities.ShoppingCart;
import com.trendyol.shoppingcart.entities.discounts.Campaign;
import com.trendyol.shoppingcart.entities.discounts.Coupon;
import com.trendyol.shoppingcart.entities.discounts.DiscountType;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * Created by Okan ERDOGAN on 2019-09-30.
 */
public class ShoppingCartPresenter {

    List<Product> products = new ArrayList<>();
    TreeMap<String, Category> categoryMap = new TreeMap<>();

    public List<Product> createProducts() {
        products.clear();
        categoryMap.clear();
        Category clothesCategory = new Category("Giyim", R.drawable.clothes_icon);
        Category shoesCategory = new Category("Ayakkabı", R.drawable.shoes_icon);
        Category electronicCategory = new Category("Elektronik", R.drawable.electronic_icon);
        Category watchAndAccessoryCategory = new Category("Saat & Aksesuar", R.drawable.watch_icon);

        categoryMap.put(clothesCategory.getTitle(), clothesCategory);
        categoryMap.put(shoesCategory.getTitle(), shoesCategory);
        categoryMap.put(electronicCategory.getTitle(), electronicCategory);
        categoryMap.put(watchAndAccessoryCategory.getTitle(), watchAndAccessoryCategory);


        Product manShirt = new Product("Pierre Cardin (Erkek Gömlek)", 89.95, clothesCategory);
        Product womenJean = new Product("Defacto (Kadın Yanı Parlak Baskılı Jean)", 79.99, clothesCategory);
        Product womenDress = new Product("Mekafashion (Kadın Üst Payet Bordro Abiye)", 199.90, clothesCategory);
        Product manSportShoes = new Product("Reebok (Erkek Günlük Ayakkabı)", 392.99, shoesCategory);
        Product womenShoes = new Product("Soho (Füme Kırmızı Siyah Kadın Topuklu Ayakkabı)", 44.99, shoesCategory);
        Product womenShoes2 = new Product("Du Jour Paris (Siyah Kadın Topuklu Ayakkabı)", 59.99, shoesCategory);
        Product computer = new Product("HP (HP Intel Core I3)", 1999, electronicCategory);
        Product mobilPhone = new Product("Xiaomi (Redmi Note7 3GB Ram)", 1442, electronicCategory);
        Product sunglasses = new Product("Polo55 (Unisex Güneş Gözlüğü)", 24.99, watchAndAccessoryCategory);
        Product wallet = new Product("United Colors Of Benetton (Siyah Kadın Cüzdan)", 82.37, watchAndAccessoryCategory);
        Product belt = new Product("Jack & Jones (Kemer - Harry Belt)", 54.99, watchAndAccessoryCategory);

        products.add(manShirt);
        products.add(womenJean);
        products.add(womenDress);
        products.add(manSportShoes);
        products.add(womenShoes);
        products.add(womenShoes2);
        products.add(computer);
        products.add(mobilPhone);
        products.add(sunglasses);
        products.add(wallet);
        products.add(belt);

        return products;
    }


    public void createDiscounts(ShoppingCart shoppingCart) {

        shoppingCart.getDiscountList().clear();

        Campaign campaign1 = new Campaign(categoryMap.get("Giyim"), 20.0, 3, DiscountType.Rate);

        Campaign campaign2 = new Campaign(categoryMap.get("Giyim"), 50.0, 5, DiscountType.Rate);

        Campaign campaign3 = new Campaign(categoryMap.get("Giyim"), 5.0, 5, DiscountType.Amount);
        
        Coupon coupon = new Coupon(100, 10, DiscountType.Rate);
        shoppingCart.applyDiscounts(campaign1, campaign2, campaign3, coupon);
    }


    public void updateProductList(ShoppingCart shoppingCart) {

        if (shoppingCart.getProductsMap().size() > 0) {
            for (Product product : shoppingCart.getProductsMap().values()) {
                for (Product product1 : getProducts()) {
                    if (product.getTitle().equals(product1.getTitle())) {
                        product1.setCount(product.getCount());
                        product1.setSelected(product.isSelected());
                    }
                }
            }

        } else {
            for (Product product1 : getProducts()) {
                product1.setCount(0);
                product1.setSelected(false);
            }
        }
    }


    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
