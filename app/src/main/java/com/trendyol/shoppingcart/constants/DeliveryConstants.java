package com.trendyol.shoppingcart.constants;

/**
 * Created by Okan ERDOGAN on 2019-09-30.
 */
public class DeliveryConstants {
    public static final double COST_PER_DELIVERY = 5.0;
    public static final double COST_PER_PRODUCT = 3.0;
    public static final double COST_FIXED = 2.99;
}
