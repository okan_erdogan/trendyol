package com.trendyol.shoppingcart.util;

import com.trendyol.shoppingcart.entities.Product;
import com.trendyol.shoppingcart.entities.ShoppingCart;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Okan ERDOGAN on 2019-09-29.
 */
public class DeliveryCostCalculator {
    private static Set<String> uniqueCategories = new LinkedHashSet<>();
    private static Set<String> uniqueProducts = new LinkedHashSet<>();


    public static double calculateDeliveryCost(ShoppingCart shoppingCart, double costPerDelivery, double costPerProduct, double fixedCost) {

        int deliveryCount = 0;
        int productCount = 0;

        uniqueCategories.clear();
        uniqueProducts.clear();

        for (Product product : shoppingCart.getProductsMap().values()) {

            if (uniqueCategories.add(product.getCategory().getTitle())) {
                deliveryCount++;
            }

            if (uniqueProducts.add(product.getTitle())) {
                productCount++;
            }
        }

        double deliveryCost = (deliveryCount * costPerDelivery) + (productCount * costPerProduct + fixedCost);
        return deliveryCost;
    }
}
